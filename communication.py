#!/usr/bin/env python2.7
"""
Created on 2018-11-04.

@author: Seshagiri Prabhu
"""
import json
from struct import pack, unpack
import logging

BUFFER_SIZE = 4096


def send(channel, message_dict):
    """Send a packed message.

    Function performs the following operations:
    1. Convert the Python dictionary `message_dict` to JSON data.
    2. Pack the JSON data length to a binary executable 32-bit
        unsigned int for the ease of sending over the socket.
    3. Send the (packed) length of the JSON data.
    4. Send the JSON data completely.

    Attributes
    ----------
        channel (:obj:`Socket`): Python Socket with the receiver.
        message_dict (dict): Python dictionary object to be send.

    """
    json_data = json.dumps(message_dict).encode("utf-8")

    # Send the JSON payload length. Packed as BE 32-bit unsigned int
    channel.sendall(pack(">I", len(json_data)))

    # Send the JSON payload itself
    channel.sendall(json_data)


def receive(channel):
    """Receives an incoming messages and unpacks it.

    Function performs following operations:
    1. Receives the 32-bit packed length of the JSON data.
    2. Unpacks the 32-bit unsigned int packed length of the JSON data.
    3. Receive the JSON data part by part if the length of the data is
        greater than `BUFFER_SIZE`.
    4. Unload the JSON data to Python dictionary

    Attributes
    ----------
        channel (:obj:`Socket`): Python Socket with the receiver.

    Returns
    -------
        message_dict (dict): Python dictionary object.

    """

    # Read first 32-bits
    json_length_string = channel.recv(4)

    if not json_length_string:
        return ''

    # Unpack 32-bits into get the JSON length
    json_length = unpack(">I", json_length_string)[0]

    json_data = b""
    while len(json_data) != json_length:
        chunk = channel.recv(min(BUFFER_SIZE, json_length - len(json_data)))

        # No data, possibly broken connection/bad protocol
        if not chunk:
            logging.error("Socket closed")
            return ''

        json_data += chunk

    # Extract the JSON payload and send the JSON object to the application
    return json.loads(json_data.decode("utf-8"))
