#!/usr/bin/env python2.7
"""
Created on 2018-11-03.

@author: Seshagiri Prabhu
"""
import os
import sys
import select
import socket
import logging
import cPickle as pickle
from threading import Thread
from datetime import datetime

# third party
from Crypto.PublicKey import RSA

# local python import
from client_info import Client, HELP_TEXT, ClientInfo
from communication import send, receive
from crypto_helper import verify_and_decrypt_rsa_message, AESCipher

# Server TCP address:port constants
HOST = "0.0.0.0"
PORT = 7171

# Constants specific to logging
LOG_FORMAT = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
ROOT_LOGGER = logging.getLogger()
CONSOLE_HANDLER = logging.StreamHandler(sys.stdout)

CONSOLE_HANDLER.setFormatter(LOG_FORMAT)
ROOT_LOGGER.addHandler(CONSOLE_HANDLER)
ROOT_LOGGER.setLevel(logging.DEBUG)


class ChatServer(Thread):
    """Chat Server.

    A multi-threaded Chat Server instance.

    Attributes
    ----------
        client_info_list (list:`ClientInfo` objects): A list to store the
            list of `Client` objects of the online users.
        outputs (list:`Socket` objects): A list of python `Socket` objects.
        host (str): IP address of the server,
            assigned value - `SERVER_ADDR`.
        port (int): TCP port number used by server,
            assigned value - `SERVER_PORT`.
        sock (:obj:'Socket'): Python Socket server listens to.
        private_key (:obj:`RSA.generate()`): RSA private key of the server.
        public_key (:obj:`RSA.PublicKey()`): RSA public key of the server.

    """

    def __init__(self, host, port):
        """Initialize a new ChatServer."""
        Thread.__init__(self)

        # `ClientInfo` class object list
        self.client_info_list = []

        # Output socket list
        self.outputs = []

        # Server host ip address
        self.host = host

        # Server port
        self.port = port

        # Flag set for listening (while loop)
        self.running = True

        # Server socket variable
        self.sock = None

        # Generate server private-key
        self.private_key = RSA.generate(2048, os.urandom)
        logging.debug('Server 2048 bit private key generated')

        # Generate server public-key
        self.public_key = self.private_key.publickey()
        logging.debug('Server 2048 bit public key generated')

        try:
            # Create TCP server socket
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            logging.debug("Server socket created successfully.")

            # Bind socket to port
            self.sock.bind((self.host, self.port))
            logging.debug("Socket bound to port: %d.", self.port)

        except socket.error, msg:
            logging.error("Failed to create/bind socket to the port.")
            logging.error(msg)
            sys.exit(0)

        """
        `SO_REUSEADDR` options is set to avoid error:
            `socket.error: [Errno 98] Address already in use`
        This is because the previous execution has left the socket in a
        `TIME_WAIT` state, and can't be immediately reused."""
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Maximum 4 clients can connect to server at a time.
        self.sock.listen(4)
        logging.debug("Listening to clients...")

    def listen(self):
        """Listen to incoming socket connections.

        Listens to the read events in the list of sockets in `inputs`
        using Python `select()` and performs following operations:

        1. Server socket `receive` event.
        2. Server socket `send` event.

        """
        # Set of input sockets to be listened
        inputs = [self.sock]

        # Set of output sockets to be listened
        self.outputs = []

        while self.running:
            try:
                """Get the list sockets which are ready to be read through
                select. The select has a timeout of 60 seconds."""
                ready_to_read = select.select(inputs, self.outputs, [], 60)[0]

            except select.error:
                break

            except socket.error:
                break

            for sock in ready_to_read:
                # Handle the server socket
                if sock == self.sock:
                    try:
                        # Accept the client socket `connect` request
                        client, address = self.sock.accept()

                        # Receive RSA public key from client
                        client_pubic_key = RSA.importKey(receive(client))

                        # Send Server's RSA public key to the client
                        send(client, self.public_key.exportKey())

                        # Receive AES key from client
                        message = verify_and_decrypt_rsa_message(
                            client_pubic_key, receive(client), self.private_key
                        )

                        # AES key's signature is verified
                        if message[0]:
                            logging.debug(
                                'Received signed AES key from client'
                            )

                        # AES key's signature cannot be verified
                        else:
                            logging.debug(
                                'Received unsigned AES key from client'
                            )

                        # Create `AESCipher` object using received AES key
                        client_aes_cipher = AESCipher(message[1])

                        # Read the login name send as encrypted message
                        data = client_aes_cipher.decrypt(receive(client))
                        client_name = data.split('NAME: ')[1]

                        """Create `ClientInfo` class instance using the
                        socket `address`, `client_name` and
                        `client_pubic_key`."""
                        client_info = ClientInfo(
                            address, client_name, client_pubic_key
                        )

                        """Create `Client` class instance using the client
                        `Socket`, `client_aes_cipher` and `client_info`
                        object."""
                        client_object = Client(
                            client, client_aes_cipher, client_info
                        )

                        # Inform all the online users about the new user
                        self.broadcast_new_user_online(client_name)

                        # Add the client socket to `inputs` list
                        inputs.append(client)

                        # Add the client socket to `outputs` list
                        self.outputs.append(client)

                        # Add the `client_info` object to `client_info_list`
                        self.client_info_list.append(client_object)

                        # Send the names of users who are online
                        message_dict = self.create_dictionary_list_of_users(
                            client_name
                        )

                        logging.debug(message_dict)

                        # Send the list of online users to the client
                        send(client, message_dict)

                    except socket.error:
                        break

                    else:
                        logging.debug("New client %s (%s:%d) is online",
                                      client_name, address[0], address[1])

                # Handle client sockets
                else:
                    try:
                        # Expecting a dictionary response from client
                        jdata = receive(sock)

                        # No data, possibly broken connection/bad protocol
                        if not jdata:
                            self.handle_user_offline(sock)
                            inputs.remove(sock)
                            sock.close()
                            continue

                        else:
                            """Create response dictionary based on the received
                            dictionary."""
                            message_dict = self.create_dict_response(jdata)

                            # Recepient is not server
                            if message_dict['status'] > 99:
                                # Set `sock` as the socket of the client
                                sock = self.get_socket_from_name(
                                    message_dict['receiver']
                                )

                            logging.debug(message_dict)

                            # Send response to the client
                            send(sock, message_dict)

                    except socket.error, msg:
                        logging.error(msg)
                        self.handle_user_offline(sock)
                        inputs.remove(sock)
                        sock.close()
                        continue

        self.stop()

    def stop(self):
        """Stop the server.

        Server is stopped by unseeting the "running" flag before closing the
        socket connection.
        """
        self.running = False
        self.sock.close()

    def broadcast_message_online_users(self, message_dict):
        """Send a message to all online users.

        Function sends the `message_dict` to all the online users.

        `message` field in the dictionary is in plain text.
        Function encrypts the message using the AES key of the
        receiver.

        Attributes
        ----------
            `message_dict` (dict): A Python dictionary object.

        """
        # Only broadcast if there is any online users.
        if self.client_info_list:
            for client_object in self.client_info_list:
                # Set the receiver as `client_info.nickname`
                message_dict['receiver'] = client_object.get_client_name()

                # Encrypt the message using `client.aes_cipher` instance
                encrypted_message = client_object.get_aes_cipher().encrypt(
                    message_dict['message']
                )

                # Set the `message` field in dictionary as encrypted message
                message_dict['message'] = encrypted_message

                # Send the message to the client.
                send(client_object.get_client_socket(), message_dict)

    def broadcast_new_user_online(self, client_name):
        """Broadcast new user online activity.

        Inform all the online users about the new user online.
        Creates the dictionary for the user online activity.
        Calls `broadcast_message_online_users` function.

        Attributes
        ----------
            client_name (str): Name of the new online client.

        """
        # Response dictionary for user online activity
        message_dict = {
            'status': 3,
            'sender': 'server',
            'receiver': '',
            'message': client_name + ' is now online.',
            'send_time': str(datetime.now())
        }

        # Function to broadcast `message_dict` to all online users
        self.broadcast_message_online_users(message_dict)

    def broadcast_user_offline(self, client_name):
        """Broadcast user offline activity.

        Inform all the online users about the user offline activity.

        Attributes
        ----------
            client_name (str):  Name of the client who has gone offline.

        """
        # Response dictionary for user offline activity
        message_dict = {
            'status': 4,
            'sender': 'server',
            'receiver': '',
            'message': client_name + ' has gone offline.',
            'send_time': str(datetime.now())
        }

        # Function to broadcast `message_dict` to all online users
        self.broadcast_message_online_users(message_dict)

    def handle_user_offline(self, sock):
        """Handle the client offline activity.

        Attributes
        ----------
            sock (:obj:`Socket`): Python `Socket` instance.

        """
        self.outputs.remove(sock)

        try:
            # Retrieve the `client_object` using the `Socket` instance
            client_object = self.get_client_object_from_sock(sock)

            # Get the name of the user who has gone offline
            client_name = client_object.get_client_name()

            logging.debug(
                'User %s (%s:%d) is now offline',
                client_object.get_client_name(),
                client_object.get_client_address()[0],
                client_object.get_client_address()[1]
            )

            # Remove the `client_object` from the `client_info_list`
            self.client_info_list.remove(client_object)

            # Inform all online users about the user going offline.
            self.broadcast_user_offline(client_name)

        except ValueError:
            logging.debug("Client object was there not present in the list")

    def create_dictionary_list_of_users(self, receiver):
        """Create dictionary for list of users.

        Attributes
        ----------
            receiver (str): Name of the receiver.

        Returns
        -------
            `message_dict` (dict): A dictionary with pickled list of users as
                message, encrypted using receiver's AES symmetric key.

        """
        # Response dictionary for `list users` request from client
        message_dict = {
            'status': 0,
            'sender': 'server',
            'receiver': receiver,
            'message': self.create_aes_encrypted_message(
                pickle.dumps(self.get_online_user_list()), receiver
            ),
            'send_time': str(datetime.now())
        }

        return message_dict

    def create_dict_response(self, jdata):
        """Create dictionary response.

        Based on the received dictionary `jdata`, function creates
        response messages. Encrypts them using the reciever's
        `receiver_aes_key`.

        For certain `status` code in `jdata`, server sends
        acknowledgment response.

        Attributes
        ----------
            jdata (dict): A received python dictionary object.

        Returns
        -------
            message_dict (dict): Python dictionary object as a response.

        """
        # Response dictionary gets the same value as that of received `jdata`
        message_dict = {
            'status': jdata['status'],
            'sender': jdata['sender'],
            'receiver': jdata['receiver'],
            'message': jdata['message'],
            'send_time': jdata['send_time']
        }

        # User requests for the list of online users
        if jdata['status'] == 0:
            # Create dictionary response using the sender's AES key
            message_dict = self.create_dictionary_list_of_users(
                jdata['sender']
            )

            # Set the time as current time as its response from server
            message_dict['send_time'] = str(datetime.now())

        # User sending message to server.
        elif jdata['status'] == 2:
            # Receiver should be the sender of the original message
            message_dict['receiver'] = jdata['sender']

            # Sender is set as `server`
            message_dict['sender'] = 'server'

            # Create encrypted message using sender's AES key
            message_dict['message'] = self.create_aes_encrypted_message(
                HELP_TEXT, jdata['sender']
            )

            # Set the time as current time as its response from server
            message_dict['send_time'] = str(datetime.now())

        # User requests to connect to an online user
        elif jdata['status'] == 100:
            # Only forward the message when the receiver is online
            if self.check_user_is_online(jdata['receiver']):
                """Include sender name in message such that the
                receiving client can use it to set `receiver` variable."""
                message = jdata['sender'] + ' wants to connect with you.'

                # Create encrypted message using receiver's AES key
                message_dict['message'] = self.create_aes_encrypted_message(
                    message, jdata['receiver']
                )

                """As the server is forwarding request, `status` code
                is not changed. Sender's RSA public key is included in a
                special field `sender_key` in `message_dict`."""
                message_dict['sender_key'] = self.get_public_key_from_name(
                    jdata['sender']
                ).exportKey()

                logging.debug(
                    '%s and %s are now connected.',
                    jdata['sender'], jdata['receiver']
                )

                # Send connection ACK to the sender
                self.send_connection_establish_ack(message_dict)

            # Inform the sender about the offline receiver
            else:
                """Include receiver name in message such that the
                receiving client can process it."""
                message = jdata['receiver'] + ' is not online.'

                # Set the status code to 102 - user gone offline
                message_dict['status'] = 102

                # Set the `sender` as `server`
                message_dict['sender'] = 'server'

                # Set the `receiver` as original `sender`
                message_dict['receiver'] = jdata['sender']

                # Encrypt the message using sender's AES key
                message_dict['message'] = self.create_aes_encrypted_message(
                    message, jdata['sender']
                )

                # Set the time as current time as its response from server
                message_dict['send_time'] = str(datetime.now())

                logging.debug(
                    '%s is trying to connect to %s who is offline.',
                    jdata['sender'], jdata['receiver']
                )

        # Client is sending encrypted AES message to another online client
        elif jdata['status'] == 103:
            # Send the ACK to the sender about the AES key forward
            self.send_aes_key_forward_ack(jdata)

        # User sending message to another online user (status == 200)
        else:
            # Send message ACK to the sender
            self.send_message_forward_ack(message_dict)

        return message_dict

    def send_aes_key_forward_ack(self, jdata):
        """Prepare the AES forward ACK dictionary.

        This function prepares the dictionary to be send to the sender who
        has request to forward a message with AES key to another online user.

        Attributes
        ----------
            jdata (dict): Python dictionary object with `status` 103.

        """
        # Response dictionary for AES key forward message
        message_dict = {
            'status': 104,
            'sender': 'server',
            'receiver': jdata['sender'],
            'message': self.create_aes_encrypted_message(
                jdata['receiver'], jdata['sender']
            ),
            'send_time': str(datetime.now())
        }

        logging.debug(message_dict)

        # Find the socket of the receiver and send the AES key response ACK
        send(self.get_socket_from_name(message_dict['receiver']), message_dict)

    def send_message_forward_ack(self, jdata):
        """Prepare the connection establishment ACK dictionary.

        This function prepares the dictionary to be send to the sender
        who has request to forward a message to an online.

        Attributes
        ----------
            jdata (dict): Python dictionary object with `status` 200.

        """
        # Response dictionary
        message_dict = {
            'status': 201,
            'sender': jdata['sender'],
            'receiver': jdata['sender'],
            'message': jdata['message'],
            'send_time': str(datetime.now())
        }

        logging.debug(message_dict)

        # Find the socket of the receiver and send the message forward ACK
        send(self.get_socket_from_name(message_dict['receiver']), message_dict)

    def send_connection_establish_ack(self, jdata):
        """Prepare the connection establish ACK dictionary.

        This function prepares the dictionary to be send to the sender
        who has request to establish a connection with an online client.

        Attributes
        ----------
            jdata (dict): Python dictionary object with `status` 100.

        """
        # Response dictionary
        message_dict = {
            'status': 101,
            'sender': 'server',
            'receiver': jdata['sender'],
            'sender_public_key': self.get_public_key_from_name(
                jdata['receiver']
            ).exportKey(),
            'message': self.create_aes_encrypted_message(
                jdata['receiver'], jdata['sender']
            ),
            'send_time': str(datetime.now())
        }

        logging.debug(message_dict)

        # Find the socket of the receiver and send the connection establish ACK
        send(self.get_socket_from_name(message_dict['receiver']), message_dict)

    def create_aes_encrypted_message(self, plain_message, receiver):
        """Create AES encrypted message.

        Function creates encrypted message for the Server using
        the AESCipher object creating using the shared AES key.

        Attributes
        ----------
            plain_message (str): Message in plain text to be encrypted.
            receiver (str): Name of the receiver.

        Returns
        -------
            b64encoded (str): A string which is encrypted using aes_cipher
                of the `receiver` and encoded using `base64`.

        """
        try:
            receiver_aescipher = self.get_client_aescipher_object(receiver)
            return receiver_aescipher.encrypt(plain_message)

        except IOError:
            logging.error('Cannot find public key for %s', receiver)
            return 'PLAIN: ' + receiver

    def check_user_is_online(self, user):
        """Check if a user is online."""
        if self.client_info_list:
            for client_object in self.client_info_list:
                if client_object.get_client_name() == user:
                    return True

        return False

    def get_online_user_list(self):
        """Get online user list.

        Return a list of the names of users who are online.

        Returns
        -------
            user_list (list): A list consists of `ClientInfo` objects.

        """
        user_list = []

        for client_object in self.client_info_list:
            user_list.append(client_object.get_client_info_object())

        return user_list

    def get_client_object_from_sock(self, sock):
        """Get Client object from client socket.

        Return the `Client` object from the `client_info_list`.

        Returns
        -------
            client_object (:obj:`Client`): `Client` class object.

        """
        for client_object in self.client_info_list:
            if client_object.get_client_socket() == sock:
                return client_object

        return None

    def get_client_object_from_name(self, name):
        """Get client object from client name.

        Return the `Client` object of `name`
        if present in client_info_list.

        Returns
        -------
            client_object (:obj:`Client`): `Client` class object or `None`.

        """
        for client_object in self.client_info_list:
            if client_object.get_client_name() == name:
                return client_object

        return None

    def get_socket_from_name(self, name):
        """Get socket from client name.

        Return the client socket.

        Returns
        -------
            sock(:obj:'Socket'): Client's `Socket` or `None`.

        """
        client_object = self.get_client_object_from_name(name)

        if client_object:
            return client_object.get_client_socket()

        return None

    def get_client_aescipher_object(self, name):
        """Get client AESCipher object from client name.

        Return the client AESCipher object.

        Returns
        -------
            aes_cipher (:obj:`AESCipher`): `AESCipher` instance or `None`.

        """
        client_object = self.get_client_object_from_name(name)

        if client_object:
            return client_object.get_aes_cipher()

        return None

    def get_public_key_from_name(self, name):
        """Get public key from client name.

        Return the public key.

        Returns
        -------
            public_key (:obj:`RSA.PublicKey()`): Public key or `None`.

        """
        client_object = self.get_client_object_from_name(name)

        if client_object:
            return client_object.get_public_key()

        return None


if __name__ == "__main__":
    ChatServer(HOST, PORT).listen()
