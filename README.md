# ClassChat
This is a simple multi-threaded terminal-based chat application for the
communication between two clients implemented using python sockets.

## Server
Server acts as a central control point. All the messages are received and forwarded to the respective receiver client by the server.

## Client
Client first connects to server, receives a list of online users and then
client starts conversation with the user.

## Communication
The communication between client-client and client-server is done over socket.
Messages are packed in JSON along with the sender, receiver, send time and status code.

## Secure Messaging
Secure messaging is a new feature added to the ClassChat in v2.0.
It uses a AES encryption scheme for secure message transmission and
RSA for secure key exchange.

### Secure Key Exchange
Client randomly generates an AES symmetric encryption key. Then, encrypts the
symmetric key with the 2048-bit RSA<sub>public</sub> key of the receiver and
signs it using its own private key. Client also transmits its public key to the
receiver.

Upon receiving the encrypted message, receiver verifies the signature,
decrypts the RSA block, which will yield the symmetric key.

This can be shown more formally as the following. Let K<sub>AES</sub> be the
randomly chosen AES key, and K<sub>Pu</sub> be the receiver's
RSA<sub>public</sub> key received from the server.

  C = E<sub>RSA</sub>(K<sub>AES</sub>, K<sub>Pu</sub>)

Let K<sub>Pr</sub> be the receiver's private RSA key. The receiver can then recover K<sub>AES</sub> as

  K<sub>AES</sub> = D<sub>RSA</sub>(C, K<sub>Pr</sub>)

### Secure Messaging
Once both users agrees on a shared AES key. Messages can be encrypted using:

  C = E<sub>AES</sub>(M, K<sub>AES</sub>)

The cipher C can be decrypted by the other using using:

  M = D<sub>AES</sub>(C<sub>1</sub>,K<sub>AES</sub>)

Use of AES enables encryption and decryption of large messages. RSA has
limitations for large text encryption.

## Secure Infrastructure
Every clients and server generates an individual 2048-bit (RSA<sub>public</sub>,
RSA<sub>private</sub>) pairs. Server saves a copy of RSA<sub>public</sub> key
of all the online clients.

Clients generates AES symmetric key each for server and other clients to whom
it wants to talk to. AES and RSA keys are exchanged at the beginning of
establishing connections. AES symmetric key exchange is performed using the
RSA<sub>public</sub> of the receiver key. Server sends the RSA<sub>public</sub>
key of the user whom the client wants to communicate.


### Sample JSON:
```JSON
message_dict = {
    'status': 200,
    'sender': 'Alice',
    'receiver': 'Bob',
    'message': 'Hello',
    'send_time': '2018-11-08 21:59:34.528833'
}
```
server-client and client-client `message` in `message_dict` is encrypted
using AES symmetric shared key.

When the client is trying to connect to another user, it sends its public
key in `sender_public_key` field in `message_dict`.

### Status Codes
| Status | Message Type                                                                                                           |
|--------|------------------------------------------------------------------------------------------------------------------------|
| 0      | User requests for a list of online users |
| 1      | Server response with a list of online user request |
| 2      | User send arbitrary message to server |
| 3      | Server broadcast a new user online message |
| 4      | Server broadcast a user gone offline  |
| 100    | User request to connect to an online user,  Include RSA public key in `sender_key` field of `message_dict`. |
| 101    | Server ACK user connection request with an online user. The public key of the user trying to connect is included. |
| 102    | Server response when user tries to connect to an offline user. |
| 103    | Client send the AES key with the user trying to connect. |
| 104    | Server ACK AES forwarding request [103] to the sender. |
| 200    | Client send a message to another client. |
| 201    | Server ACK message forwarding request. |

## Run the program
This program will only work in Linux-based systems as the
server/client uses `select()` synchronous I/O function
which is a system-call only implemented in Linux kernel.

The `server.py` and `client.py` is written in Python `v2.7.15`.

### Linux (debian) requirements
As the `server.py` and `client.py` uses a lot of UNIX utilities and threading,
`python-dev` package is required to run the programs.

```bash
sudo apt-get install python-dev
```
### Install Python Requirements
The only third-party python package used is `pycrypto`.

```bash
pip install -r requirements.txt
```
### Server Program
`server.py` binds to port `7171`.

```bash
python server.py
```

### Client Program
`client.py` connects to server port `7171` running in the `localhost`.
```bash
python client.py
```
Client program will prompt the user for a nickname and then establishes
connection with server.

Below is the terminal output of two instances of clients running in
different terminals simultaneously.

#### Useful Chat Commands
```bash
1. list users
    -- lists the number of users who are currently online.
2. message USER
    -- connects to the <USER> to start conversation.
3. quit
    -- Exit the chat client
```

## Screenshots
### Client Instance 1
User `blob`
![client-1](img/client1.png)

### Client Instance 2
User `blob123` connecting to `blob`.
![client-2](img/client2.png)

### Server Program
![server-1](img/server1.png)
![server-2](img/server2.png)

### Server Error Handling Capability
Server can detect if the client (`admin`) is trying to connect to
an offline user or non-existing user (`root`).
#### Client Instance
![client-3](img/client3.png)
#### Server Program
![server-3](img/server3.png)
