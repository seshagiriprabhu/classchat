#!/usr/bin/env python2.7
"""
Created on 2018-11-08.

@author: Seshagiri Prabhu
"""
import logging
from base64 import b64encode, b64decode

# third party
from Crypto import Random
from Crypto.Signature import PKCS1_PSS
from Crypto.Hash import SHA
from Crypto.Cipher import AES

BS = 16
PAD = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
UNPAD = lambda s: s[0:-ord(s[-1])]


class AESCipher(object):
    """This is a class for AES symmetric cipher.

    Attributes
    ----------
        key (:str: `os.urandom(16)`): The AES symmetric key.

    """

    def __init__(self, key):
        """Init for `AESCipher` class.

        Parameters
        ----------
            key (str): The AES symmetric key.

        """
        self.key = key

    def get_aes_key(self):
        """Return AES key.

        Returns
        -------
            key (str): The AES symmetric key.

        """
        return self.key

    def encrypt(self, raw):
        """Encrypt given data using AES.

        Parameters
        ----------
            raw (str): The message that needs to be encrypted.

        Returns
        -------
            b64encode (str): `base64` encoded string with 16 bytes
                `initialization_vector` followed by AES encrypted message.

        """
        raw = PAD(raw)
        intialization_vector = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, intialization_vector)
        return b64encode(intialization_vector + cipher.encrypt(raw))

    def decrypt(self, enc):
        """Decrypt AES encrypted data.

        Parameters
        ----------
            enc (str): The message that needs to be decrypted.

        Returns
        -------
            UNPAD (str): Unpaded decrypted string.

        """
        enc = b64decode(enc)
        intialization_vector = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, intialization_vector)
        return UNPAD(cipher.decrypt(enc[16:]))


def encrypt_and_sign_rsa_message(
        sender_private_key, message, receiver_public_key
):
    """Encrypt and sign message.

    Encrypts the given message using the receiver's RSA public_key
    and sign the message hash using client's RSA private key.

    Parameters
    ----------
        sender_private_key (:obj:`RSA.generate()`): The private key of the
            sender. Also to be used to sign the encrypted message.
        message (str): The message that needs to be encrypted.
        receiver_public_key (:obj:`RSA.PublicKey()`): The key to be used to
            decrypt the message.

    Returns
    -------
        b64encode (str): `base64` encoded RSA encrypted using receiver's
        public key and signed using sender's private key.

    """
    try:
        # Encrypt
        message = receiver_public_key.encrypt(message, 0)
        message = message[0]

        message_hash = SHA.new()
        message_hash.update(message)

        # Append Signature
        signer = PKCS1_PSS.new(sender_private_key)
        signature = signer.sign(message_hash)
        message = "%s#^[[%s" % (message, signature)

        # Encode the message in base64
        message = b64encode(message)

    except ValueError:
        logging.error('Too large text, cannot encrypt.')
        message = 'PLAIN: ' + message

    return message


def verify_rsa_message_signature(sender_public_key, message, signature):
    """Verify the signed message.

    Verifies the signature included in the message by calculating the
    hash of the received encrypted message.

    Parameters
    ----------
        sender_public_key (:obj:`RSA.PublicKey()`): The key used to verify
            the signature.
        message (str): The encrypted message encoded using base64.
        signature (str): Signature of the encrypted message.

    Returns
    -------
        `True` if the signature is verifiable, `False` otherwise.

    """
    try:
        message_hash = SHA.new()
        message_hash.update(message)

        verifier = PKCS1_PSS.new(sender_public_key)
        return verifier.verify(message_hash, signature)

    except IOError:
        logging.error("Invalid message signature.")
        return False


def verify_and_decrypt_rsa_message(sender_public_key, message, private_key):
    """Verify and decrypt the RSA message.

    Signature verification of the received RSA message using the public key
    of the sender. Decryption of the message using the reciever's private key.

    Parameters
    ----------
        sender_public_key (:obj:`RSA.PublicKey()`): The key used to verify
            the signature.
        message (str): The encrypted message encoded using `base64`.
        private_key (:obj:`RSA.Priva()`): The private key to be used to
            decrypt message.

    Returns
    -------
        message (str): Decrypted message.

    """
    decoded_message = b64decode(message)
    dataparts = decoded_message.split('#^[[')
    signature = dataparts[1]
    message = dataparts[0]

    verified = verify_rsa_message_signature(
        sender_public_key, message, signature
    )

    message = private_key.decrypt(message)

    if message != '\x00':
        if verified:
            message = (True, message)

        else:
            message = (False, message)

    return message
